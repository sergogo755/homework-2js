// Асинхронность в JS - это дейсвие которое выполнится через какое - то время, или при определенных событиях.Пример Асинхронности в JS
// setTimeout,Promise,асинхронные функции. 

let btn = document.querySelector(".findIP")
let url = "https://api.ipify.org/?format=json"
let ulList = document.querySelector(".root")
async function ayncIP() {
  const response = await fetch(url)
  const data = await response.json()
  console.log(data.ip);
  const IPfound = await fetch(`http://ip-api.com/json/${data.ip}`)
  const ff = await IPfound.json()
  console.log(ff)
  ulList.innerHTML = `
  <li> Country: ${ff.country}</li>
  <li>Timezon: ${ff.timezone}</li>
  <li>Region: ${ff.regionName}  ${ff.region}</li>
  <li>Timezon: ${ff.city}</li>
  `
}

btn.addEventListener("click", ayncIP)
